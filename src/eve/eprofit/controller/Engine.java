package eve.eprofit.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import eve.eprofit.model.Blueprint;
import eve.eprofit.model.Item;
import eve.eprofit.model.MarketGroup;

public class Engine {

	public static void main( String args[] ) throws Exception{
		Engine engine = Engine.getSingleton();
		engine.updatePrices();
	}

	public static Engine singleton;
	private HashMap<Integer, Blueprint> blueprintsMap;
	private HashMap<Integer, Item> itemsMap;
	private HashMap<Integer, MarketGroup> marketGroupsMap;

	public HashMap<Integer, Blueprint> getBlueprintsMap() {
		return blueprintsMap;
	}

	public HashMap<Integer, Item> getItemsMap() {
		return itemsMap;
	}

	public HashMap<Integer, MarketGroup> getMarketGroupsMap() {
		return marketGroupsMap;
	}

	public static Engine getSingleton() throws FileNotFoundException, IOException, ClassNotFoundException{
		if(singleton == null){
			singleton = new Engine();
		}
		return singleton;
	}

	@SuppressWarnings("unchecked")
	private Engine() throws FileNotFoundException, IOException, ClassNotFoundException{
		File file = new File("Data/Blueprints.dat");
		ObjectInputStream input = new ObjectInputStream(new FileInputStream(file));
		blueprintsMap = (HashMap<Integer, Blueprint>) input.readObject();
		input.close();

		file = new File("Data/Items.dat");
		input = new ObjectInputStream(new FileInputStream(file));
		itemsMap = (HashMap<Integer, Item>) input.readObject();
		input.close();

		file = new File("Data/MarketGroups.dat");
		input = new ObjectInputStream(new FileInputStream(file));
		marketGroupsMap = (HashMap<Integer, MarketGroup>) input.readObject();
		input.close();
	}

	public void updatePrices() throws ParserConfigurationException, SAXException, IOException{
		int counter = 0;
		String address = "http://api.eve-central.com/api/marketstat?";
		for(Item item : itemsMap.values()){
			if(counter < 150){
				address += "&typeid=" + item.getItemID();
				counter++;
			}
			else{
				getPrices(address);
				address = "http://api.eve-central.com/api/marketstat?&typeid=" + item.getItemID();
				counter = 1;
			}
		}
		getPrices(address);
		
		File file = new File("Data/Items.dat");
		try ( ObjectOutputStream out = new ObjectOutputStream( new FileOutputStream( file ))) {
			out.writeObject ( itemsMap );
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		System.out.println("updated prices!");
	}

	private void getPrices(String address) throws ParserConfigurationException, SAXException, IOException{
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(address);
		doc.getDocumentElement().normalize();
		NodeList nList = doc.getElementsByTagName("type");
		for (int temp = 0; temp < nList.getLength(); temp++) {
			Node nNode = nList.item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				int id = Integer.parseInt(eElement.getAttribute("id"));
				double price = Double.parseDouble(eElement.getElementsByTagName("percentile")
						.item(1).getTextContent());
				if(price > 0) itemsMap.get(id).setPrice(price);
				System.out.println(itemsMap.get(id).getName() + ": " + price);
			}
		}
	}
}
