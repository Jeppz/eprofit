package eve.eprofit.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import eve.eprofit.model.Blueprint;
import eve.eprofit.model.Item;
import eve.eprofit.model.MarketGroup;

public class Filebuilder {

	public static void main( String args[] ){
		buildFiles();
	}

	private static void buildFiles(){

		ArrayList<Integer> interestingMarketGroups = new ArrayList<>();
		HashMap<Integer,Item> items = new HashMap<>();
		HashMap<Integer, Blueprint> blueprints = new HashMap<>();
		HashMap<Integer, MarketGroup> mainMarketGroups = new HashMap<>();
		HashMap<Integer, MarketGroup> allMarketGroups = new HashMap<>();

		try {
			Class.forName("org.sqlite.JDBC");
			//Requires "sqlite-latest.sqlite" file in same folder, available from https://www.fuzzwork.co.uk/dump/
			Connection c = DriverManager.getConnection("jdbc:sqlite:sqlite-latest.sqlite");
			c.setAutoCommit(false);
			Statement stmt = c.createStatement();

			
			
			//builds list of market groups
			ResultSet marketGroupIDTable = stmt.executeQuery( "select marketgroupid, parentgroupid, marketgroupname from invmarketGroups;" );
			while(marketGroupIDTable.next()){
				int groupID = marketGroupIDTable.getInt("marketGroupID");
				int parentID = marketGroupIDTable.getInt("parentgroupid");
				String groupName = marketGroupIDTable.getString("marketGroupName");
				MarketGroup mg = new MarketGroup(groupID, groupName, parentID);
				allMarketGroups.put(groupID, mg);
			}

			for(MarketGroup mg : allMarketGroups.values()){
				if(mg.getParentID() > 0) allMarketGroups.get(mg.getParentID()).addSubGroup(mg);
				else mainMarketGroups.put(mg.getGroupID(), mg);
			}

			Integer[] parentGroups = {2,4,9,11,19,24,150,157,475,477,955,1849};
			interestingMarketGroups.addAll(Arrays.asList(parentGroups));
			for(int i : parentGroups){
				for(MarketGroup mg : getSubGroups(mainMarketGroups.get(i))){
					interestingMarketGroups.add(mg.getGroupID());
				}
			}

			
			
			//Build items map
			ResultSet invTypesTable = stmt.executeQuery( "SELECT * FROM invTypes;" );
			while ( invTypesTable.next() ) {
				int marketGroup = invTypesTable.getInt("marketGroupID");

				for(int i : interestingMarketGroups){
					if(marketGroup == i){
						int id = invTypesTable.getInt("typeID");
						String  name = invTypesTable.getString("typeName");
						float  size = invTypesTable.getFloat("volume");
						Item item = new Item(id, size, name, marketGroup);
						item.setPrice(0);
						items.put(id, item);
					}
				}	
			}



			//Build blueprints map
			ResultSet blueprintsTable = stmt.executeQuery("select a.typeID, a.activityID, b.producttypeid as outputID, "
					+ "b.quantity as outputQuantity, a.time, c.materialtypeid as materialID, c.quantity as materialQuantity, "
					+"d.probability, e.skillID, e.level as skillLevel, f.maxProductionlimit as maxRuns "
					+"from industryActivity as a "
					+"left join industryActivityProducts as b on a.typeID = b.typeID and a.activityID = b.activityID "
					+"left join industryActivityMaterials as c on a.typeID = c.typeID and a.activityID = c.activityID "
					+"left join industryActivityProbabilities as d on a.typeID = d.typeID and a.activityID = d.activityID "
					+"left join industryActivitySkills as e on a.typeID = e.typeID and a.activityID = e.activityID "
					+"left join industryBlueprints as f on a.typeID = f.typeID");
			while(blueprintsTable.next()){
				int activityID = blueprintsTable.getInt("activityID");
				int typeID = blueprintsTable.getInt("typeID");
				Blueprint bp;

				if(!blueprints.containsKey(typeID)){
					bp = new Blueprint(typeID);
					blueprints.put(typeID, bp);
				}
				else bp = blueprints.get(typeID);

				switch (activityID){
				case 1:
					//1 Manufacturing
					bp.setProductionTime(blueprintsTable.getInt("time"));
					bp.addProduct(blueprintsTable.getInt("outputID"), blueprintsTable.getInt("outputQuantity"));
					bp.addMaterial(blueprintsTable.getInt("materialID"), blueprintsTable.getInt("materialQuantity"));
					bp.addRequiredManufacturingSkill(blueprintsTable.getInt("skillID"), blueprintsTable.getInt("skillLevel"));
					bp.setBpcMaxRuns(blueprintsTable.getInt("maxRuns"));
					break;
				case 2:
					//2 Researching Technology 

					break;
				case 3:
					//3 Researching Time Productivity
					bp.setResearchTimeTime(blueprintsTable.getInt("time"));
					break;
				case 4:
					//4 Researching Material Productivity
					bp.setResearchMaterialTime(blueprintsTable.getInt("time"));
					break;
				case 5:
					//5 Copying
					bp.setCopyTime(blueprintsTable.getInt("time"));
					break;
				case 6:
					//6 Duplicating 

					break;
				case 7:
					//7 Reverse Engineering 

					break;
				case 8:
					//8 Invention
					bp.addInventionProduct(blueprintsTable.getInt("outputID"), blueprintsTable.getInt("outputQuantity"));
					bp.setInventionTime(blueprintsTable.getInt("time"));
					bp.addInventionMaterial(blueprintsTable.getInt("materialID"), blueprintsTable.getInt("materialQuantity"));
					bp.setInventionProbability(blueprintsTable.getDouble("probability"));
					bp.addRequiredInventionSkill(blueprintsTable.getInt("skillID"),blueprintsTable.getInt("skillLevel"));
					break;
				default:
					break;
				}
			}

			//Writes the files from the hashmaps we made
			File file = new File("Data/Items.dat");
			ObjectOutputStream out = new ObjectOutputStream( new FileOutputStream( file ));
			out.writeObject ( items );
			out.close();
			System.out.println(items.size() + " Items saved");

			file = new File("Data/Blueprints.dat");
			out = new ObjectOutputStream( new FileOutputStream( file ));
			out.writeObject ( blueprints );
			out.close();
			System.out.println(blueprints.size() + " Blueprints saved");

			file = new File("Data/MarketGroups.dat");
			out = new ObjectOutputStream( new FileOutputStream( file ));
			out.writeObject ( mainMarketGroups );
			out.close();
			System.out.println(mainMarketGroups.size() + " Main market groups saved");

		} catch(Exception e){
			e.printStackTrace();
		}
	}

	//Returns all sub market groups
	private static ArrayList<MarketGroup> getSubGroups(MarketGroup mg){
		ArrayList<MarketGroup> allSubs = new ArrayList<>();
		if(mg.getSubGroups().size() > 0) allSubs.addAll(getDeeperSubgroups(mg.getSubGroups()));
		allSubs.addAll(mg.getSubGroups());
		return allSubs;
	}

	//Submethod for getSubGroups that goes deeper into the sub groups
	private static ArrayList<MarketGroup> getDeeperSubgroups(ArrayList<MarketGroup> arrMg){
		ArrayList<MarketGroup> allSubs = new ArrayList<>();
		for(MarketGroup mg : arrMg){
			if(mg.getSubGroups().size() > 0) allSubs.addAll(getDeeperSubgroups(mg.getSubGroups()));
			allSubs.addAll(mg.getSubGroups());
		}
		return allSubs;
	}
}