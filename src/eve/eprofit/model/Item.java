package eve.eprofit.model;

import java.io.Serializable;
import java.util.ArrayList;


public class Item implements Serializable{

	private static final long serialVersionUID = -6465934643460207033L;
	
	private int itemID;
	private double size;
	private double price;
	private int blueprintID;
	private String name;
	private ArrayList<Integer> marketGroup;
	
	public Item(int itemID, double size, String name, int marketGroup) {
		this.marketGroup = new ArrayList<>();
		this.marketGroup.add(marketGroup);
		this.itemID = itemID;
		this.size = size;
		this.name = name;
	}

	@Override
	public String toString() {
		return "Item [name=" + name + "]";
	}

	public int getItemID() {
		return itemID;
	}

	public void setItemID(int itemID) {
		this.itemID = itemID;
	}

	public double getSize() {
		return size;
	}

	public void setSize(double size) {
		this.size = size;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getBlueprintID() {
		return blueprintID;
	}

	public void setBlueprintID(int blueprintID) {
		this.blueprintID = blueprintID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Integer> getMarketGroup() {
		return marketGroup;
	}

	public void setMarketGroup(ArrayList<Integer> marketGroup) {
		this.marketGroup = marketGroup;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
}

