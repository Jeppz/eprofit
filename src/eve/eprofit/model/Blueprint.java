package eve.eprofit.model;

import java.io.Serializable;
import java.util.HashMap;



public class Blueprint implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5646260855640072705L;
	private final int blueprintItemID;
	private String blueprintName;
	private final HashMap<Integer, Integer> materials;
	private final HashMap<Integer, Integer> product;
	private int productionTime;
	private int researchMaterialTime;
	private int researchTimeTime;
	private int copyTime;
	private int inventionTime;
	private final HashMap<Integer, Integer> inventionMaterials;
	private final HashMap<Integer, Integer> inventionOutput;
	private double inventionProbability;
	private int bpcMaxRuns;
	private final HashMap<Integer, Integer> requiredManufacturingSkills;
	private final HashMap<Integer, Integer> requiredInventionSkills;
	
	public Blueprint(int blueprintItemID){
		this.blueprintItemID = blueprintItemID;
		materials = new HashMap<>();
		product = new HashMap<>();
		requiredManufacturingSkills = new HashMap<>();
		inventionOutput = new HashMap<>();
		inventionMaterials = new HashMap<>();
		requiredInventionSkills = new HashMap<>();
	}

	
	
	//http://eve-industry.org/export/IndustryFormulas.pdf
	public HashMap<Integer, Double> getCalculatedMaterials(int ME, int TE, int runs){
		HashMap<Integer, Double> returnMap = new HashMap<>();
		for(int i : materials.keySet()){
			returnMap.put(i, Math.max(runs, Math.ceil(Math.round(runs * materials.get(i) * ME))));	
		}
		 return returnMap;
	}
	
	public double getCalculatedProductionTime(int runs, double skillMod, double timeMod){
		return productionTime * timeMod * skillMod * runs;
	}
	
	
	
	
	
	public void addMaterial(int id, int amount){
		materials.put(id, amount);
	}
	
	public void addProduct(int id, int amount){
		product.put(id, amount);
	}
	
	public void addInventionProduct(int id, int amount){
		inventionOutput.put(id, amount);
	}
	
	public void addRequiredManufacturingSkill(int skillID, int skillLevel){
		requiredManufacturingSkills.put(skillID, skillLevel);
	}
	
	public void addInventionMaterial(int id, int amount){
		inventionMaterials.put(id, amount);
	}
	
	public void addRequiredInventionSkill(int id, int level){
		requiredInventionSkills.put(id, level);
	}
	
	
	
	
	
	
	public HashMap<Integer, Integer> getRequiredInventionSkills() {
		return requiredInventionSkills;
	}

	public HashMap<Integer, Integer> getInventionMaterials() {
		return inventionMaterials;
	}

	public HashMap<Integer, Integer> getProduct() {
		return product;
	}

	public HashMap<Integer, Integer> getInventionOutput() {
		return inventionOutput;
	}

	public HashMap<Integer, Integer> getRequiredManufacturingSkills() {
		return requiredManufacturingSkills;
	}

	public HashMap<Integer, Integer> getMaterials(){
		return materials;
	}
	
	public HashMap<Integer, Integer> getProducts(){
		return product;
	}
	
	public int getBlueprintItemID() {
		return blueprintItemID;
	}

	public String getBlueprintName() {
		return blueprintName;
	}

	public void setBlueprintName(String blueprintName) {
		this.blueprintName = blueprintName;
	}

	public int getProductionTime() {
		return productionTime;
	}

	public void setProductionTime(int productionTime) {
		this.productionTime = productionTime;
	}

	public int getResearchMaterialTime() {
		return researchMaterialTime;
	}

	public void setResearchMaterialTime(int researchMaterialTime) {
		this.researchMaterialTime = researchMaterialTime;
	}

	public int getResearchTimeTime() {
		return researchTimeTime;
	}

	public void setResearchTimeTime(int researchTimeTime) {
		this.researchTimeTime = researchTimeTime;
	}

	public int getCopyTime() {
		return copyTime;
	}

	public void setCopyTime(int copyTime) {
		this.copyTime = copyTime;
	}

	public int getInventionTime() {
		return inventionTime;
	}

	public void setInventionTime(int inventionTime) {
		this.inventionTime = inventionTime;
	}

	public double getInventionProbability() {
		return inventionProbability;
	}

	public void setInventionProbability(double d) {
		this.inventionProbability = d;
	}

	public int getBpcMaxRuns() {
		return bpcMaxRuns;
	}

	public void setBpcMaxRuns(int bpcMaxRuns) {
		this.bpcMaxRuns = bpcMaxRuns;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	

}
