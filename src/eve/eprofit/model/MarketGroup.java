package eve.eprofit.model;

import java.io.Serializable;
import java.util.ArrayList;


public class MarketGroup implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6827162875034929067L;
	private int groupID;
	private String groupName;
	private int parentID;
	ArrayList<MarketGroup> subGroups;
	
	public MarketGroup(int groupID, String groupName, int parentID) {
		this.groupID = groupID;
		this.groupName = groupName;
		this.parentID = parentID;
		subGroups = new ArrayList<>();
	}

	@Override
	public String toString() {
		return "MarketGroup [groupName=" + groupName + "]";
	}

	public void addSubGroup(MarketGroup mg){
		subGroups.add(mg);
	}
	
	public int getGroupID() {
		return groupID;
	}

	public void setGroupID(int groupID) {
		this.groupID = groupID;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public int getParentID() {
		return parentID;
	}

	public void setParentID(int parentID) {
		this.parentID = parentID;
	}

	public ArrayList<MarketGroup> getSubGroups() {
		return subGroups;
	}

	public void setSubGroups(ArrayList<MarketGroup> subGroups) {
		this.subGroups = subGroups;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
